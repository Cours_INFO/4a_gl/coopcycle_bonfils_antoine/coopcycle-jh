# coopcycle-jh

Ce dépôt contient un .jdl pour importer l'application et un .jh pour les entités.

***

* **[coopcycle-app.jdl](./coopcycle-app.jdl)**: fichier généré avec la commande `jhipster export-jdl coopcycle-app.jdl`.

* **[coopcycle-entities.jh](./coopcycle-entities.jh)**: fichier écrit à la main.

***

Ces fichiers sont à importer avec les commandes suivantes:

* coopcycle-app:

    ```UNIX
    wget https://gitlab.com/Cours_INFO/4a_gl/coopcycle_bonfils_antoine/coopcycle-jh/-/raw/main/coopcycle-app.jdl
    jhipster import-jdl ./coopcycle-app.jdl
    ```

* coopcycle-entities:

    ```UNIX
    wget https://gitlab.com/Cours_INFO/4a_gl/coopcycle_bonfils_antoine/coopcycle-jh/-/raw/main/coopcycle-entities.jdl
    jhipster import-jdl ./coopcycle-entities.jh
    ```
